import Vue from 'vue'
import Router from 'vue-router'
import MerchantCard from '@/components/MerchantCard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MerchantCard',
      component: MerchantCard
    }
  ]
})
